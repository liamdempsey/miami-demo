<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<?php // Adds the tagline to the Services page/

		 	if(get_field('services_tagline')) {
			echo '<p>' . get_field('services_tagline') . '</p>' .

			// Adds the team members and their email addresses to the Services page.
			'<h2 class="our-team">' . 'Our Team' . '</h2>' .
			'<p>'. '<a href="mailto:' . esc_attr(get_field('email_address_no_1')) . '">' . get_field('name_of_person_no_1') . '</a>' . '<br />' .
			'<a href="mailto:' . esc_attr(get_field('email_address_no_2')) . '">' . get_field('name_of_person_no_2') . '</a></p>';
		}
		?>
		<?php twentyseventeen_edit_link( get_the_ID() ); ?>
	</header><!-- .entry-header -->
	<div class="entry-content">

		<?php // Adds the business address and details to the Contact page.

			if (get_field('business_name')) {
			echo '<h2>' . get_field('business_name') . '</h2>';
			echo '<p>' . get_field('business_address_line_1') . '<br />' .
			get_field('business_address_line_2') . '<br />' .
			get_field('business_city') . ', ' . get_field('business_state') . ' ' . get_field('business_zip') . '</p>';

			echo '<p>' . '<strong>' . 'Tel: ' . '</strong>' . get_field('business_phone') . '</p>';
			echo '<p>'  . '<strong>' . 'Email: ' . '</strong>' .  get_field('business_email_address') . '</a>' . '</p>';

		}
		?>

		<?php // Add the quote to the About page.

			if (get_field('quote')) {
			echo '<p class="quote">' . '<em>' . get_field('quote') . '</em>' . '</p>';
			echo '<p class="quote-author">' . '– ' . get_field('quote_author') . '</p>';
		}
		?>


		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
